import java.util.LinkedList;

public class Stack {
    LinkedList<Integer> isi;

    public Stack(){
        isi = new LinkedList();

    }

    public void push(int s){
        isi.addFirst(s);
    }

    public boolean pop (){
        if(isi.isEmpty())
            return false;
        else{
            isi.removeFirst();
            return true;
        }
    }

    public void printStack(){
        for(int s : isi){
            System.out.print(s);
        }
        System.out.println;
    }


}
